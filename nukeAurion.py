#!/usr/bin/env python


# Step one for migrating from Aurion 4.1 to CONNECT 4.4.1
# is to nuke Aurion. Separate script will handle migration.
#
# Reference: https://devtools.mirthcorp.com/confluence/pages/viewpage.action?pageId=10191367
#
# 9/2015
# Author: John Downing 

#yay

import re
import subprocess
import sys
import time

deprecationFolder = '/root/aurionNukeBackup/'

print'****************************************************************'
print 'The next steps will be irreversible. I\'m going to rpm uninstall, delete files & database things.'
print 'If you stop now, nothing will happen. No one needs to know.'
print'****************************************************************'

letsGo = 'f'

while letsGo != 'y':
        letsGo = raw_input('Am I clear to nuke Aurion once and for all? (y/n): ')
        if letsGo == 'n':
			print 'Terminating.'
			sys.exit()
        elif letsGo != 'n' and letsGo != 'y':
			letsGo = raw_input('y/n only please: ')
        else:
			letsGo = 'y'

#rpm uninstall
print 'RPM uninstalling...'
subprocess.call('rpm -e mirth-aurion', shell=True)
print 'DONE.'

#undeploy NHINC
print 'Undeploying NHINC41Adapters from Glassfish (Aurion) (Enterprise Application)...'
subprocess.call('/opt/aurion/bin/asadmin undeploy NHINC41Adapters', shell=True)
print 'DONE.'

#stopping Aurion
print 'Stopping Aurion...'
subprocess.call('service mirth-aurion stop', shell=True)
print 'DONE.'

#create a working directory
print 'Creating backup directory & subdirectories at '+deprecationFolder
subprocess.call('mkdir '+deprecationFolder, shell=True)
print('DONE.')

#moves /opt/aurion to /opt/aurionDEPRECATED in order to prevent Aurion from (hopefully) conflicting with CONNECT while maintaining the original configuration for quick recovery
print 'Moving /opt/aurion to '+deprecationFolder
subprocess.call('mv /opt/aurion '+deprecationFolder, shell=True)
print 'DONE.'

#move adapter.properties back to retain any XDS connections
print'Restoring Aurion\'s adapter.properties to preserve XDS connections'
subprocess.call('mkdir -p /opt/aurion/domains/nssdomain/config/nhin', shell=True)
subprocess.call('cp '+deprecationFolder+'/aurion/domains/nssdomain/config/nhin/adapter.properties /opt/aurion/domains/nssdomain/config/nhin', shell=True)
print 'DONE.'

#same for /opt/cp-aurion
print 'Moving /opt/cp-aurion to'+deprecationFolder+'...'
subprocess.call('mv /opt/cp-aurion '+deprecationFolder, shell=True)
print 'DONE.'

#restart apache for... some reason 
print 'Restarting apache...'
subprocess.call('service mirth-apache restart',shell=True)
print 'DONE.'

#edit /etc/sysconfig/iptables to remove aurion entries
print 'Backing up & removing /etc/sysconfig/iptables entries....'
	#backup
subprocess.call('cp /etc/sysconfig/iptables /etc/sysconfig/iptables.bak.'+time.strftime("%Y-%m-%d_%H:%M:%S"), shell=True)

	#write a new file to then swap with the existing file. It maintains root ownership
newIptables = open('/etc/sysconfig/iptablesNew','w')

for line in open('/etc/sysconfig/iptables'):
	if 'aurion' not in line:
		newIptables.write(line)

subprocess.call('rm -rf /etc/sysconfig/iptables', shell=True)
subprocess.call('mv /etc/sysconfig/iptablesNew /etc/sysconfig/iptables', shell=True)
print 'DONE.'

#drop nhinc db
print 'Dropping mysql database "nhincdb"...'
subprocess.call('mysql -uroot -pNHIE-Gateway -e \'drop database nhincdb;\' > '+deprecationFolder+'dbDropOutput.txt',shell=True)
subprocess.call('cat '+deprecationFolder+'dbDropOutput.txt',shell=True)
print 'DONE.'

#moving /opt/nhinc to avoid potential environment variable conflicts with old Aurion and new CONNECT (asked stevek)
print 'Moving /opt/nhinc to '+deprecationFolder+'in order to avoid environment variable conflicts'
subprocess.call('mv /opt/nhinc '+deprecationFolder,shell=True)
print 'DONE.'

#remove the /etc/init.d/mirth-aurion
#actually..... I'll back it up.
print 'Deprecating (moving) /etc/init.d/mirth-aurion to '+deprecationFolder+'...'
subprocess.call('mv /etc/init.d/mirth-aurion '+deprecationFolder+'mirth-aurion.bak.'+time.strftime("%Y-%m-%d_%H:%M:%S"),shell=True)
print 'DONE.'

print'****************************************************************'
print'This concludes Aurion nuking. Please install CONNECT 4.4.1 and run migration script.'
print'****************************************************************'

