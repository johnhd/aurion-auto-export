#!/usr/bin/env python

# Steps to check for Success
# cd /opt/
# rm -rf connect
# tar -xf connect.tar
# ' + domain1ConfigDirectory + 'nhin
# /opt/cp-connect/htdocs/connect/gateways/index.xml
# rm -rf index.xml
# /root/dev-aurion...something/AurionSometingSomethingScript/aurion41toconnect441.python
# /opt/connect/glassfish3/glassfish/domains/domain1/config/nhin/nternalConnectionInfo.xml
#
import re
import sys
import time
import getopt
import fileinput
import logging
import os.path
import os
from xml.dom import minidom
from subprocess import call

# Global Variables
# - deprecationFolder was created in the prior script
deprecationFolder = '/root/aurionNukeBackup/aurion/'
workingFolder = '/root/aurion-auto-export-working-folder/'
publicCertsFolder = workingFolder + 'partnerPublicCerts/'
keypairFolder = workingFolder+'keypair/'
#domainsConfigDirectory = '/opt/connect/glassfish3/glassfish/domains/config/' NOT USED
domain1ConfigDirectory = '/opt/connect/glassfish3/glassfish/domains/domain1/config/'
gatewayIndexPath = '/opt/cp-connect/htdocs/connect/gateways/index.xml'
localHcid = ''

# logMessage
# - Helper function to print out log messages
def logMessage(message, type=None):
	if type == 'warning':
		logging.warning(message)
	elif type == 'error':
		logging.error(message)
	else:
		logging.info(message)

# createDirectories
# - Creates the directories in which the output files will live
def createDirectories():
	logMessage('Creating Directories: /root/aurion-auto-export-working-folder & subdirectories')
	call('mkdir '+ workingFolder, shell=True)
	call('mkdir '+ publicCertsFolder, shell=True)
	call('mkdir '+ keypairFolder, shell=True)
	call('mkdir -p /opt/cp-connect/htdocs/connect/gateways/', shell=True)
	logMessage('Directories Created')

# updatejars - Deprecate after APPLIANCE-1649 is complete
# - Patches .jar files from local directory
# - Moves the old .jar to the local directory
# - Copies new .jar file
# - Apply appropriate ownership to the .jar
def updateJars():
	logMessage('Patching CONNECT .jars using the script\'s local directory. Remove when APPLIANCE-1649 is complete ( D:< DEREK ELDER)')
	connectLibrary = '/opt/connect/glassfish3/glassfish/domains/domain1/applications/CONNECT-GF-4.4.1/lib/'
	call('mv ' + connectLibrary + 'CONNECTCoreLib-4.4.1.jar .', shell=True)
	call('cp ./*.jar ' + connectLibrary, shell=True)
	call('chown mirth:mirth ' + connectLibrary + '*.jar', shell=True)
	logMessage('Patching Complete')

# certGeneration
# - Move default gateway.jks and cacert.jks (Mostly filled with clutter)
# - Uses same keypair and certificates provided by Aurion
# - Export public keys into /root/aurion-auto-export/partnerPublicCerts
# - Import certiticates into new cacert.jks
def certGeneration():
	logMessage('Generating Certificates')
	call('mv ' + domain1ConfigDirectory + 'gateway.jks ' + domain1ConfigDirectory + 'gateway.jks.outTheBox', shell=True)
	call('mv ' + domain1ConfigDirectory + 'cacerts.jks ' + domain1ConfigDirectory + 'cacerts.jks.outTheBox', shell=True)

	publicCertList = 'certAliases.txt'

	logMessage('Exporting public certitficates from Aurion')
	call('certutil -d ' + deprecationFolder + '/domains/nssdomain/config -L > ' + publicCertsFolder+publicCertList, shell=True)
	logMessage('Export Complete')

	input_file = open(publicCertsFolder+publicCertList, 'r')
	publicCertCount = 0
	for certificate in input_file:
		if certificate.strip() and "Certificate Nickname" not in certificate.strip() and "SSL,S/MIME,JAR/XPI" not in certificate.strip():
			cert_alias =  certificate.split(' ', 1)[0]
			# - Change the public cert for the gateway cert to gateway from s1as
			# - Just for the filename, since we need to use the original alias to actually export the file
			if(cert_alias == 's1as'):
				cert_alias_name = 'gateway'
			else:
				cert_alias_name = cert_alias
			exportedCertFile = publicCertsFolder+cert_alias_name + '.pem'
			call('certutil -d ' + deprecationFolder + '/domains/nssdomain/config -L -n ' + cert_alias + ' -a > ' + exportedCertFile, shell=True)
			call('keytool -import -trustcacerts -alias ' + cert_alias_name + ' -file ' + exportedCertFile +' -keystore ' + domain1ConfigDirectory + 'cacerts.jks -storepass changeit -noprompt', shell=True)
			logMessage('Adding: ' + cert_alias_name)
			publicCertCount += 1
	logMessage('Exported ' + str(publicCertCount) + ' public partner certificates to ' + publicCertsFolder)

	keyAlias = 's1as'
	logMessage('Exporting private keypair for ' + keyAlias)
	defaultKeyPass = 'changeit'
	call('pk12util -o ' + keypairFolder + 'keypair.p12 -n s1as -d ' + deprecationFolder + 'domains/nssdomain/config/ -K ' + defaultKeyPass + ' -W ' + defaultKeyPass, shell=True)
	call('openssl pkcs12 -in ' + keypairFolder + 'keypair.p12 -out ' + keypairFolder + 'keypair.pem -password pass:' + defaultKeyPass + ' -passout pass:' + defaultKeyPass, shell=True)
	call('keytool -exportcert -alias s1as -keystore ' + keypairFolder + 'keypair.p12 -storetype pkcs12 -rfc -file ' + keypairFolder + 'head.pem -storepass '+defaultKeyPass, shell=True)

	# - Saving subject line for the public key pair and import them to the new keystore
	call('openssl x509 -text -noout -in ' + keypairFolder + 'head.pem | sed -n -e \'s/Subject: //p\' > ' + keypairFolder + 'publicKeySubject.txt', shell=True)
	call('keytool -importkeystore -srckeystore ' + keypairFolder + 'keypair.p12 -destkeystore ' + domain1ConfigDirectory + 'gateway.jks -srcalias s1as -destalias gateway -srcstoretype pkcs12 -srcstorepass changeit -deststorepass changeit -noprompt', shell=True)
	logMessage('Certificates Generated')

# createNode
# - Helper node creation function
def createNode(nodeName, nodeAttributes=None, nodeValue=None, nodeChildren=None):
	tempDom = minidom.Document()
	newNode = tempDom.createElement(nodeName)
	if nodeValue:
		nodeValue = tempDom.createTextNode(str(nodeValue))
		newNode.appendChild(nodeValue)
	if nodeAttributes is not None:
		for attribute in nodeAttributes:
			newNode.setAttribute(attribute[0], attribute[1])
	if nodeChildren is not None:
		for child in nodeChildren:
			newNode.appendChild(child)
	return newNode

# generateInternalConnectionInfo
# - Grabs OIDs and endpoints from internalConnectionInfo and UDDIconnectionInfo
# - Outputs file to uddiConnectionInfo
def generateInternalConnectionInfo():
	logMessage('Migrating partners from internalConnectionInfo.xml (Name, OID, Services, etc)')
	internalConnectionPath = domain1ConfigDirectory + 'nhin/'

	logMessage('Deleting default internalConnectionInfo from ' + internalConnectionPath, 'warning')
	call('rm -rf ' + internalConnectionPath + 'internalConnectionInfo.xml', shell=True)
	logMessage('Default internalConnectionInfo Deleted')

	logMessage('Pasting over template internalConnectionInfo to ' + internalConnectionPath)
	newInternalConnection = internalConnectionPath + 'internalConnectionInfo.xml'
	call('cp internalConnectionInfoGood.xml ' + newInternalConnection, shell=True)
	logMessage('New internalConnectionInfo.xml created')

	try:
		newConnectionDom = minidom.parse(newInternalConnection)
	except:
		logMessage('Could not read: %s' % newInternalConnection, 'error')
		sys.exit(1)
	try:
		oldConnectionDom = minidom.parse(deprecationFolder + '/domains/nssdomain/config/nhin/internalConnectionInfo.xml')
	except:
		logMessage('Could not read: %s/domains/nssdomain/config/nhin/internalConnectionInfo.xml' % deprecationFolder, 'error')
		sys.exit(1)
	internalConnectionNodes = oldConnectionDom.getElementsByTagName('internalConnectionInfo')

	internalConnectionPartnersCount = 0
	totalServicesCount = 0
	handledLocalGateway = False
	gatewayDom = minidom.Document()
	gatewayList = createNode('list')
	gatewayDom.appendChild(gatewayList)

	for internalConnectionEntry in internalConnectionNodes:
		connectionName = internalConnectionEntry.getElementsByTagName('name')[0].firstChild.data
		internalConnectionPartnersCount += 1
		logMessage('Exporting: ' + connectionName)

		isLocalGateway = True
		if connectionName != 'Local Gateway':
			isLocalGateway = False

		if isLocalGateway == False:
			homeCommId = internalConnectionEntry.getElementsByTagName('homeCommunityId')[0].firstChild.data
			businessName = createNode('name', [['xml:lang', 'en']], connectionName)

#11/13
			#identifierBag = createNode('identifierBag', None, None, [createNode('keyedReference', [['tModelKey', 'uddi:nhin:nhie:homecommunityid'], ['keyName', 'homecommunityid'], ['keyValue', 'urn:oid:' + str(homeCommId)]])])

			gatewayInfo = createNode('gatewayInfo', None, None, [createNode('identifier', None, connectionName), createNode('label', None, connectionName), createNode('oid', None, homeCommId), createNode('description', None, connectionName), createNode('tags', None, connectionName)])

			gatewayList.appendChild(gatewayInfo)

			businessServicesNode = createNode('businessServices')
			services = internalConnectionEntry.getElementsByTagName('service')
			for service in services:
				totalServicesCount += 1

				serviceName = service.getElementsByTagName('name')[0].firstChild.data
				serviceEndpoint = service.getElementsByTagName('endpointURL')[0].firstChild.data

				if serviceName == 'PatientDiscovery':
					bindingTemplateKeyValue = '1.0'
				else:
					bindingTemplateKeyValue = '2.0'

				#aurion uses QueryForDocument CONNECT uses QueryForDocuments. NOTICE THE EXTRA S
				if serviceName == 'QueryForDocument':
					serviceName = 'QueryForDocuments'

				#same for RetrieveDocument
				if serviceName == 'RetrieveDocument':
					serviceName = 'RetrieveDocuments'

				bindingTemplateCategoryBag = createNode('categoryBag', None, None, [createNode('keyedReference', [['tModelKey','uddi:nhin:versionofservice'], ['keyName',''], ['keyValue', bindingTemplateKeyValue]])])
				serviceAccessPointNode = createNode('accessPoint', [['useType','endPoint']], serviceEndpoint)
				serviceBindingTemplateNode = createNode('bindingTemplate', [['bindingKey','uddi:nhincnode:' + serviceName], ['serviceKey','uddi:nhincnode:' + serviceName]], None, [serviceAccessPointNode, bindingTemplateCategoryBag])
				serviceBindingTemplatesNode = createNode('bindingTemplates', None, None, [serviceBindingTemplateNode])
				serviceCategoryBag = createNode('categoryBag', None, None, [createNode('keyedReference', [['tModelKey','uddi:nhin:standard-servicenames'], ['keyName',serviceName], ['keyValue',serviceName]])])
				serviceNode = createNode('businessService', [['businessKey', 'uddi:testnhieonenode:' + homeCommId], ['serviceKey', 'uddi:nhincnode:' + serviceName]], None, [createNode('name', [['xml:lang', 'en']], serviceName), serviceBindingTemplatesNode, serviceCategoryBag])
				businessServicesNode.appendChild(serviceNode)

			businessEntityNodeIdentifierBag = createNode('identifierBag', None, None, [createNode('keyedReference', [['tModelKey','uddi:nhin:nhie:homecommunityid'], ['keyName',''], ['keyValue',homeCommId]])])
			businessEntityNodeCategoryBag = createNode('categoryBag', None, None, [createNode('keyedReference', [['tModelKey','uddi:uddi.org:ubr:categorization:iso3166'], ['keyName','United States'], ['keyValue','US']])])


#11/13 removed the identifierbag after businessName! 
#may need to add bacK? Not sure if redundant?
			businessEntity = createNode('businessEntity', [['businessKey', 'uddi:nhincnode:' + homeCommId]], None, [businessName, businessServicesNode, businessEntityNodeIdentifierBag, businessEntityNodeCategoryBag])
			newConnectionDom.childNodes[0].appendChild(businessEntity)
		else:
			logMessage('This is the local ID, ignoring everything except the name and OID and using the template')
			global localHcid
			localHcid = internalConnectionEntry.getElementsByTagName('homeCommunityId')[0].firstChild.data
			handledLocalGateway = True

	#this is an error state. May as well stop now
	if(handledLocalGateway == False):
		logMessage('\'Local Gateway\' not found in Aurion gateway.properties\nFind gateway.properties and rename the file.\nTerminating.', 'error')
		sys.exit()

	try:
		newInternalConnectionFile = open(newInternalConnection, 'w+')
		newInternalConnectionFile.write(newConnectionDom.toxml())
		newInternalConnectionFile.close()
	except:
		logMessage('Could not create: %s' % newInternalConnection, 'error')
		sys.exit(1)

			#we skipped this earlier, we need to insert our local oid. Laziness dicates file-based regex.
			#Moved from the else part of the loop above (~line 210)
	if(handledLocalGateway==True):
		try:
			newInternalConnectionFile = open(newInternalConnection, 'r+')
			data = newInternalConnectionFile.read()
			newInternalConnectionFile.truncate()
			newInternalConnectionFile.seek(0,0)
			newInternalConnectionFile.write(re.sub("LOCALOIDHERE", localHcid, data))
			newInternalConnectionFile.close()
		except:
			logMessage('Could not modify: %s' % newInternalConnection, 'error')
			sys.exit(1)

	try:
		gatewayIndexFile = open(gatewayIndexPath, 'w')
		gatewayIndexFile.write(gatewayDom.toxml())
		gatewayIndexFile.close()
	except:
		logMessage('Could not create: %s' % gatewayIndexPath, 'error')
		sys.exit(1)

	call('xmllint --format ' + newInternalConnection + ' --output ' + newInternalConnection, shell=True)
	call('xmllint --format ' + gatewayIndexPath + ' --output ' + gatewayIndexPath, shell=True)

	logMessage('Exported a total of ' + str(internalConnectionPartnersCount) + ' partners, with a combined total of ' + str(totalServicesCount) + ' services.')


#parse through the subject line we broke out of the previous keypair's x509 output to get the subject attributes
def migrateKeyPairSubject(inputPubKeySubject,attribute):
	if attribute in inputPubKeySubject:
		index = inputPubKeySubject.find(attribute)+len(attribute)
		return inputPubKeySubject[index:(inputPubKeySubject.find(",",index))]
	else:
		return ''

# migrateAdapterProperties
# - Let's open adapter.properties, both old and new
def migrateAdapterProperties():
	# THIS is misleading... copypasta from the nuke aurion script?
	#logMessage('Migrating adapter.properties. Returning the original back to '+deprecationFolder+ 'domains/nssdomain/config/nhin/ in order to preserve XDS functionality!') 

	connectAdapterProperties = domain1ConfigDirectory + 'nhin/adapter.properties'
	aurionAdapterProperties = deprecationFolder + 'domains/nssdomain/config/nhin/adapter.properties'

	#DomainsToOIDsMap=whatIwant^theOidShouldAlreadyBeReplaced 
	inputAurionAdapterPropertiesFile = open(aurionAdapterProperties, 'r')
	inputAurionAdapterProperties = inputAurionAdapterPropertiesFile.read()
	inputAurionAdapterPropertiesFile.close()

	siteID = inputAurionAdapterProperties[inputAurionAdapterProperties.find("DomainsToOIDsMap=")+17:inputAurionAdapterProperties.find("^")]
	if(siteID != ''):
		logMessage('Internal Site ID discovered to use in CONNECT adapter.properties: ' + siteID)
	else:
		logMessage('Action Needed: SiteID was found for Aurion\'s adapter.properties\' DomainsToOIDsMap line.', 'warning')
		logMessage('Defaulting to \'SITE1\'\nFIND the correct SITE ID and paste it in to CONNECT adapter.properties!', 'warning')
		siteID = 'SITE1'

	assertionIssuer=inputAurionAdapterProperties[inputAurionAdapterProperties.find('AssertionIssuer=CN=')+19:]
	assertionIssuer = assertionIssuer.split(',')[0]
	if(assertionIssuer != ''):
		logMessage('External DNS discovered to use in CONNECT adapter.properties: ' + assertionIssuer)
	else:
		logMessage('Action Needed: No external DNS was found for adapter.properties\' assertionIssuer line. Will append everything else, leaving an empty CN.', 'warning')
		logMessage('FIND the external DNS and paste it in to CONNECT adapter.properties', 'warning')

	#we need our own information from our keypair, which we saved earlier in keypairFolder+"publicKeySubject.txt"
	inputPubKeySubjectFile = open(keypairFolder + 'publicKeySubject.txt','r')
	inputPubKeySubject = inputPubKeySubjectFile.read()
	inputPubKeySubjectFile.close()

	assertionAuthzCN = migrateKeyPairSubject(inputPubKeySubject, "CN=")
	assertionAuthzOU = migrateKeyPairSubject(inputPubKeySubject, "OU=")
	assertionAuthzO = migrateKeyPairSubject(inputPubKeySubject, "O=")
	assertionAuthzL = migrateKeyPairSubject(inputPubKeySubject, "L=")
	assertionAuthzST = migrateKeyPairSubject(inputPubKeySubject, "ST=")
	assertionAuthzC = migrateKeyPairSubject(inputPubKeySubject, "C=")

	logMessage('Adding Line: AssertionAuthzDecisionStatementIssuer=CN=' + assertionAuthzCN + ',OU=' + assertionAuthzOU + ',O=' + assertionAuthzO + ',L=' + assertionAuthzL + ',ST=' + assertionAuthzST + ',C=' + assertionAuthzC)
	logMessage('Adding Line: AssertionIssuer=CN=' + assertionIssuer + ',OU=Mirth Dev,O=Mirth,L=Costa Mesa,ST=CA,C=US')

	#replace on the 100.100 OID and site using regex/simple replace
	#build up the new AssertionAuthz and Assertion Issuer lines and append them
	connectAdapterPropertiesFile = open(connectAdapterProperties,"r+b")
	connectAdapterPropertiesTemp = re.sub("100\.100", localHcid, connectAdapterPropertiesFile.read())
	connectAdapterPropertiesTemp = re.sub("SITE1", siteID, connectAdapterPropertiesTemp)
	#adding logic to take into account the ADAPTERS now read in MirthResultsServerURL. We need to populate it with wherever MR is. 
	#ASSUMING LOCALHOST
	connectAdapterPropertiesTemp = re.sub("MirthResultsServerURL=http://localhost:8888","MirthResultsServerURL=https://localhost:11443", connectAdapterPropertiesTemp)



	connectAdapterPropertiesFile.truncate()
	connectAdapterPropertiesFile.seek(0,0)
	connectAdapterPropertiesFile.write(connectAdapterPropertiesTemp)
	connectAdapterPropertiesFile.write('AssertionAuthzDecisionStatementIssuer=CN=' + assertionAuthzCN + ',OU=' + assertionAuthzOU + ',O=' + assertionAuthzO + ',L=' + assertionAuthzL + ',ST=' + assertionAuthzST + ',C=' + assertionAuthzC + '\n')
	connectAdapterPropertiesFile.write('AssertionIssuer=CN=' + assertionIssuer + ',OU=Mirth Dev,O=Mirth,L=Costa Mesa,ST=CA,C=US\n')
	connectAdapterPropertiesFile.close()

	logMessage('adapter.properties migrated')

# modifyGatewayOid
def modifyGatewayOid():
	logMessage('Adding local OID (' + localHcid + ') to CONNECT gateway.properties')
	connectGatewayProperties = domain1ConfigDirectory + 'nhin/gateway.properties'
	connectGatewayPropertiesFile = open(connectGatewayProperties,"r+b")
	connectGatewayPropertiesTemp = re.sub("100\.100", localHcid, connectGatewayPropertiesFile.read())
	connectGatewayPropertiesFile.truncate()
	connectGatewayPropertiesFile.seek(0,0)
	connectGatewayPropertiesFile.write(connectGatewayPropertiesTemp)
	connectGatewayPropertiesFile.close()

	logMessage('Local OID (' + localHcid + ') added to CONNECT gateway.properties')

# restartCONNECT
def restartCONNECT():
	logMessage('Stopping CONNECT...')
	call('service connect stop', shell=True)
	logMessage('Stopping Results...')
	call('service mirth-mirthresults stop', shell=True)
	logMessage('Starting CONNECT...')
	call('service connect start', shell=True)
	logMessage('Starting Results...')
	call('service mirth-mirthresults start', shell=True)
	logMessage('Chowning everything in /config/nwhin to mirth:mirth')
	call('chown mirth:mirth /opt/connect/glassfish3/glassfish/domains/domain1/config/nhin/*', shell=True)
	logMessage('Process Complete.')
	logMessage('DO NOT FORGET: You still need to manually enable the UDDI for CONNECT (one file, super easy)', 'warning')

def main():
	logging.basicConfig(format='%(asctime)s - [%(levelname)s]: %(message)s', level=logging.DEBUG)
	createDirectories()
	updateJars()
	certGeneration()
	generateInternalConnectionInfo()
	migrateAdapterProperties()
	modifyGatewayOid()
	restartCONNECT()

if __name__ == '__main__':
	main()

